export const commonProjectsOfThePair = (newArray) => {
    const result = [];
    for (let i = 0; i < newArray.length; i++) {
        for (let y = 0; y < newArray.length; y++) {
            if (newArray[i].EmpID !== newArray[y].EmpID && newArray[i].ProjectID === newArray[y].ProjectID) {
                if (
                    new Date(newArray[i].DateFrom).valueOf() < new Date(newArray[y].DateFrom).valueOf() &&
                    (
                        new Date(newArray[i].DateTo).valueOf() > new Date(newArray[y].DateFrom).valueOf() ||
                        (new Date().valueOf() > new Date(newArray[y].DateFrom).valueOf() && newArray[i].DateTo === 'null')
                    )
                ) {
                    let differenceInTime = (new Date(newArray[i].DateTo).valueOf() || new Date().valueOf()) - new Date(newArray[y].DateFrom).valueOf();
                    let totalDays = differenceInTime / (1000 * 3600 * 24);
                    result.push({
                        id: newArray[i].ProjectID,
                        firstEmp: newArray[i].EmpID,
                        secondEmp: newArray[y].EmpID,
                        projectId: newArray[i].ProjectID,
                        days: totalDays.toFixed(0)
                    });
                }
            }
        }
    }
    return result;
}
