import { useState } from 'react'
import '../style.css'
import { commonProjectsOfThePair } from '../utils/commonProjectsOfThePair';
import DataTable from './Table';

function CsvReader() {
    const [csvFile, setCsvFile] = useState();
    const [csvArray, setCsvArray] = useState([]);

    const processCsv = (str, delim = ', ') => {
        const headers = str.slice(0, str.indexOf('\n')).split(delim);
        const rows = str.slice(str.indexOf('\n') + 1).split('\n');
        const newArray = rows.map(row => {
            const values = row.split(delim);
            const eachObject = headers.reduce((obj, header, i) => {
                obj[header] = values[i];

                return obj;
            }, {})

            return eachObject;
        })

        const projects = commonProjectsOfThePair(newArray)
        setCsvArray(projects);
    }

    const submit = () => {
        const file = csvFile;
        const reader = new FileReader();

        reader.onload = function (e) {
            const text = e.target.result;
            processCsv(text)
        }

        reader.readAsText(file)
    }

    return (
        <div className='table'>
            {
                csvArray.length ?
                    <DataTable csvArray={csvArray} /> :
                     <div className='input'>
                        <input
                            type='file'
                            accept='.csv'
                            id='csvFile'
                            onChange={
                                (e) => {
                                    setCsvFile(e.target.files[0])
                                }
                            }
                        >
                        </input>
                        <button className="button" onClick={(e) => {
                            e.preventDefault()
                            if (csvFile) {
                                submit()
                            }
                        }}>Submit</button>
                    </div>
            }
        </div>
    )
}

export default CsvReader;